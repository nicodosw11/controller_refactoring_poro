class BookingsController < ApplicationController
  def create
    room = Room.find_by(params[:room_id])
    start_date = Date.parse(params[:check_in])
    end_date = Date.parse(params[:check_out])
    @booking = BookingCreationService.new(room, start_date, end_date)
    if @booking.create_booking
      render json: { message: 'Booking created.' }, status: :ok
    else
      render json: { message: 'Booking conflicts with an existing booking' }, status: :unprocessable_entity
    end
  end

  private

  def booking_params
    params.permit(:check_in, :check_out, :room_id)
  end
end
