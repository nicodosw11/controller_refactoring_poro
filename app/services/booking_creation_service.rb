class BookingCreationService
  def initialize(room, start_date, end_date)
    @room = room
    @start_date = start_date
    @end_date = end_date
  end

  def create_booking
    booking = Booking.new(check_in: @start_date, check_out: @end_date, room: @room)
    if has_conflict?
      return false
    else
      booking.save
    end
  end

  private

  def has_conflict?
    conflicting_dates.any?
  end

  def conflicting_dates
   @room.bookings.where(
    "((? <= check_in AND check_in <= ?)
    OR (? <= check_out AND check_out <= ?)
    OR (check_in < ? AND ? < check_out))",
    @start_date, @end_date,
    @start_date, @end_date,
    @start_date, @end_date
    )
  end
end
