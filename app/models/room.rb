class Room < ActiveRecord::Base
  has_many :bookings
  validates_presence_of :number, :name
end
