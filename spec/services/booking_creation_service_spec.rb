require 'rails_helper'

RSpec.describe BookingCreationService, type: :service do

  let!(:renaissance) { create(:room_renaissance)}
  let!(:initial_booking) { create(:initial_booking, room: renaissance)}
  let(:valid_service) { BookingCreationService.new(renaissance, '2019-02-10', '2019-02-13') }
  let(:conflicting_start_date_service) { BookingCreationService.new(renaissance, '2019-02-14', '2019-02-16') }
  let(:conflicting_end_date_service) { BookingCreationService.new(renaissance, '2019-02-12', '2019-02-14') }
  let(:overlapping_start_date_service) { BookingCreationService.new(renaissance, '2019-02-13', '2019-02-15') }
  let(:overlapping_end_date_service) { BookingCreationService.new(renaissance, '2019-02-16', '2019-02-18') }
  let(:in_between_existing_period_service) { BookingCreationService.new(renaissance, '2019-02-15', '2019-02-16') }

  describe '#conflicting_dates' do
    context 'with no overlapping period' do
      subject(:service) { valid_service }
      it 'returns empty array' do
        expect(service.send(:conflicting_dates)).to be_empty
      end
    end

    context 'with conflicting start_date' do
      subject(:service) { conflicting_start_date_service }
      it 'returns initial booking' do
        expect(service.send(:conflicting_dates)).to match_array(initial_booking)
      end
    end

    context 'with conflicting end_date' do
      subject(:service) { conflicting_end_date_service }
      it 'returns initial booking' do
        expect(service.send(:conflicting_dates)).to match_array(initial_booking)
      end
    end

    context 'with booking period overlapping start_date' do
      subject(:service) { overlapping_start_date_service }
      it 'returns initial booking' do
        expect(service.send(:conflicting_dates)).to match_array(initial_booking)
      end
    end

    context 'with booking period overlapping end_date' do
      subject(:service) { overlapping_end_date_service }
      it 'returns initial booking' do
        expect(service.send(:conflicting_dates)).to match_array(initial_booking)
      end
    end

    context 'with booking period in between existing reservation period' do
      subject(:service) { in_between_existing_period_service }
      it 'returns initial booking' do
        expect(service.send(:conflicting_dates)).to match_array(initial_booking)
      end
    end
  end

  describe '#has_conflict?' do
    context 'with no overlapping period' do
      subject(:service) { valid_service }
      it 'returns false' do
        expect(service.send(:has_conflict?)).to be false
      end
    end

    context 'with conflicting start_date' do
      subject(:service) { conflicting_start_date_service }
      it 'returns true' do
        expect(service.send(:has_conflict?)).to be true
      end
    end

    context 'with conflicting end_date' do
      subject(:service) { conflicting_end_date_service }
      it 'returns true' do
        expect(service.send(:has_conflict?)).to be true
      end
    end

    context 'with booking period overlapping start_date' do
      subject(:service) { overlapping_start_date_service }
      it 'returns true' do
        expect(service.send(:has_conflict?)).to be true
      end
    end

    context 'with booking period overlapping end_date' do
      subject(:service) { overlapping_end_date_service }
      it 'returns true' do
        expect(service.send(:has_conflict?)).to be true
      end
    end

    context 'with booking period in between existing reservation period' do
      subject(:service) { in_between_existing_period_service }
      it 'returns true' do
        expect(service.send(:has_conflict?)).to be true
      end
    end
  end

  describe '#create_booking' do
    context 'with no overlapping period' do
      subject(:service) { valid_service }
      it "does create a booking" do
        expect {service.create_booking}.to change { Booking.count }.by(1)
      end
    end

    context 'with conflicting start_date' do
      subject(:service) { conflicting_start_date_service }
      it "does not create a booking" do
        expect {service.create_booking}.to_not change(Booking, :count)
      end
    end

    context 'with conflicting end_date' do
      subject(:service) { conflicting_end_date_service }
      it "does not create a booking" do
        expect {service.create_booking}.to_not change(Booking, :count)
      end
    end

    context 'with booking period overlapping start_date' do
      subject(:service) { overlapping_start_date_service }
      it "does not create a booking" do
        expect {service.create_booking}.to_not change(Booking, :count)
      end
    end

    context 'with booking period overlapping end_date' do
      subject(:service) { overlapping_end_date_service }
      it "does not create a booking" do
        expect {service.create_booking}.to_not change(Booking, :count)
      end
    end

    context 'with booking period in between existing reservation period' do
      subject(:service) { in_between_existing_period_service }
      it "does not create a booking" do
        expect {service.create_booking}.to_not change(Booking, :count)
      end
    end
  end
end
