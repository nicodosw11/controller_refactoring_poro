require 'rails_helper'

RSpec.describe Booking, type: :model do

  let(:renaissance) { create(:room_renaissance)}

  it { expect(Booking.reflect_on_association(:room).macro).to eq(:belongs_to) }

  it 'is valid with valid attributes' do
    expect(build(:initial_booking)).to be_valid
  end

  it 'is invalid with invalid attributes' do
    expect(Booking.new).to be_invalid
  end

  context '#temporary test' do
    it 'checking created instance' do
      booking = Booking.new(check_in: Date.parse('2019-02-18'), check_out: Date.parse('2019-02-21'), room: renaissance)
      expect(booking.check_in.to_s).to eq('2019-02-18')
      expect(booking.check_out.to_s).to eq('2019-02-21')
      expect(booking.room.name).to eq('Renaissance')
    end
  end
end
