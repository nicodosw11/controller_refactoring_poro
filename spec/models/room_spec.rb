require 'rails_helper'

RSpec.describe Room, type: :model do
  it 'is valid with valid attributes' do
    expect(build(:room_renaissance)).to be_valid
  end

  it 'is invalid with invalid attributes' do
    expect(Room.new).to be_invalid
  end
end
