require 'rails_helper'

RSpec.describe 'Create Bookings', type: :request do

   let!(:renaissance) { create(:room_renaissance)}
   let(:room_id) { renaissance.id }
   let!(:initial_booking) { create(:initial_booking, room: renaissance)}

   describe 'POST /rooms/:room_id/bookings' do
      let(:valid_attributes) { { check_in: Date.parse('2019-02-10'), check_out: Date.parse('2019-02-13'), room: renaissance } }
      let(:request) { post "/rooms/#{room_id}/bookings", params: valid_attributes }

      let(:attributes_with_conflicting_start_date) { { check_in: Date.parse('2019-02-14'), check_out: Date.parse('2019-02-16'), room: renaissance } }
      let(:request_conflicting_start_date) { post "/rooms/#{room_id}/bookings", params: attributes_with_conflicting_start_date }

      let(:attributes_with_conflicting_end_date) { { check_in: Date.parse('2019-02-12'), check_out: Date.parse('2019-02-14'), room: renaissance } }
      let(:request_conflicting_end_date) { post "/rooms/#{room_id}/bookings", params: attributes_with_conflicting_end_date }

      let(:overlapping_start_date_attributes) { { check_in: Date.parse('2019-02-13'), check_out: Date.parse('2019-02-15'), room: renaissance } }
      let(:request_overlapping_start_date) { post "/rooms/#{room_id}/bookings", params: overlapping_start_date_attributes }

      let(:overlapping_end_date_attributes) { { check_in: Date.parse('2019-02-16'), check_out: Date.parse('2019-02-18'), room: renaissance } }
      let(:request_overlapping_end_date) { post "/rooms/#{room_id}/bookings", params: overlapping_end_date_attributes }

      let(:in_between_period_attributes) { { check_in: Date.parse('2019-02-15'), check_out: Date.parse('2019-02-16'), room: renaissance } }
      let(:request_in_between_period) { post "/rooms/#{room_id}/bookings", params: in_between_period_attributes }

      context 'when the request does not contain overlapping period' do
        it 'creates a booking' do
          expect {request}.to change(Booking, :count).by(1)
        end

        it 'returns status code 200 (ok)' do
          post "/rooms/#{room_id}/bookings", params: valid_attributes
          expect(response).to have_http_status(200)
        end

        it 'returns the correct success message' do
          post "/rooms/#{room_id}/bookings", params: valid_attributes
          expect(response.body).to match(/Booking created./)
        end
      end

      context 'when the request contains conflicting start_date' do
        it 'does not create a booking' do
          expect {request_conflicting_start_date}.to_not change(Booking, :count)
        end

        it 'returns status code 422 (unprocessable_entity)' do
          post "/rooms/#{room_id}/bookings", params: attributes_with_conflicting_start_date
          expect(response).to have_http_status(422)
        end

        it 'returns failure message' do
          post "/rooms/#{room_id}/bookings", params: attributes_with_conflicting_start_date
          expect(response.body).to match(/Booking conflicts with an existing booking/)
        end
      end

      context 'when the request contains conflicting end_date' do
        it 'does not create a booking' do
          expect {request_conflicting_end_date}.to_not change(Booking, :count)
        end

        it 'returns status code 422 (unprocessable_entity)' do
          post "/rooms/#{room_id}/bookings", params: attributes_with_conflicting_end_date
          expect(response).to have_http_status(422)
        end

        it 'returns failure message' do
          post "/rooms/#{room_id}/bookings", params: attributes_with_conflicting_end_date
          expect(response.body).to match(/Booking conflicts with an existing booking/)
        end
      end

      context 'when the request contains period overlapping start_date' do
        it 'does not create a booking' do
          expect {request_overlapping_start_date}.to_not change(Booking, :count)
        end

        it 'returns status code 422 (unprocessable_entity)' do
          post "/rooms/#{room_id}/bookings", params: overlapping_start_date_attributes
          expect(response).to have_http_status(422)
        end

        it 'returns failure message' do
          post "/rooms/#{room_id}/bookings", params: overlapping_start_date_attributes
          expect(response.body).to match(/Booking conflicts with an existing booking/)
        end
      end

      context 'when the request contains period overlapping end_date' do
        it 'does not create a booking' do
          expect {request_overlapping_end_date}.to_not change(Booking, :count)
        end

        it 'returns status code 422 (unprocessable_entity)' do
          post "/rooms/#{room_id}/bookings", params: overlapping_end_date_attributes
          expect(response).to have_http_status(422)
        end

        it 'returns the correct success message' do
          post "/rooms/#{room_id}/bookings", params: overlapping_end_date_attributes
          expect(response.body).to match(/Booking conflicts with an existing booking/)
        end
      end

      context 'when the request contains period in between existing reservation period' do
        it 'does not create a booking' do
          expect {request_in_between_period}.to_not change(Booking, :count)
        end

        it 'returns status code 422 (unprocessable_entity)' do
          post "/rooms/#{room_id}/bookings", params: in_between_period_attributes
          expect(response).to have_http_status(422)
        end

        it 'returns the correct success message' do
          post "/rooms/#{room_id}/bookings", params: in_between_period_attributes
          expect(response.body).to match(/Booking conflicts with an existing booking/)
        end
      end
   end
end
