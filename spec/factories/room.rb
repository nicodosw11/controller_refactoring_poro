FactoryBot.define do
  factory :room_renaissance, class: :room do
    number { 101 }
    name { 'Renaissance' }
  end
end
