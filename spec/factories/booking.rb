FactoryBot.define do
  factory :initial_booking, class: :booking do
    check_in {Date.parse('2019-02-14')}
    check_out {Date.parse('2019-02-17')}
    association :room, factory: :room_renaissance
  end
end
